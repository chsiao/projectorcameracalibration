﻿//With credits to www.emgu.com/forum/viewtopic.php?f=7&t=421#p1334
using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Windows.Forms;

namespace Calibration
{
    public class Calibrate
    {
        private int successes = 0;
        private int n_boards = 0;
        private const int board_dt = 20;
        private int board_w;
        private int board_h;
        private int board_n;
        private System.Drawing.Size board_sz;
        private PointF[][] image_points;
        private MCvPoint3D32f[][] object_points;
        public IntrinsicCameraParameters intrinsicParam;
        public ExtrinsicCameraParameters[] extrinsicParams;


        public Calibrate(string[] args)
        {
            ArrayList argsList = new ArrayList();
            int i=0;
            try
            {
                for (i = 0; i < args.Length; i++)
                    argsList.Add(Convert.ToInt32(args[i]));
            }
            catch
            {
                MessageBox.Show("not valid numbers:" + args[i]);
            }


            this.board_w = (int)argsList[0];
            this.board_h = (int)argsList[1];
            this.n_boards = (int)argsList[2];

            this.board_sz = new System.Drawing.Size(board_w, board_h);
            this.board_n = board_w * board_h;

            this.image_points = new PointF[n_boards][];
            this.object_points = new MCvPoint3D32f[n_boards][];

            successes = 0;
        }

        public bool GenerateIntrinsicExtrinsicCameraParameters(Bitmap sourceImage, out Image<Gray, Byte> targetImage)
        {
            PointF[] corners;

            int frame = 0;

            //acquire image
            Image<Bgr, Byte> image = new Image<Bgr, byte>(sourceImage);//capture.QueryFrame();
            Image<Gray, Byte> gray_image = image.Convert<Gray, Byte>();
            targetImage = gray_image;

            if (frame++ % board_dt == 0)
            {
                //find chessboard corners
                corners = CameraCalibration.FindChessboardCorners(gray_image, board_sz, CALIB_CB_TYPE.DEFAULT);
                
                //add the chessboard to the image
                if (corners != null)
                {
                    CameraCalibration.DrawChessboardCorners(targetImage, board_sz, corners);
                    //why use if no found board? b/c check on # corners takes care of that
                    //refine the resolution
                    gray_image.FindCornerSubPix(new PointF[][] { corners }, new Size(10, 10), new Size(-1, -1), new MCvTermCriteria(0.05));

                    //add good board to data
                    if (corners.Length == board_n)
                    {
                        object_points[successes] = new MCvPoint3D32f[board_n];
                        for (int j = 0; j < board_n; ++j)
                        {
                            image_points[successes] = corners;
                            object_points[successes][j].x = j / board_w;
                            object_points[successes][j].y = j % board_w;                            
                        }
                        successes++;
                        //Console.WriteLine("Found another {0} corners. {1} to go.", corners.Length, n_boards - successes);
                    }
                }
            }

            if (successes == this.n_boards)
            {
                intrinsicParam = new IntrinsicCameraParameters();
                extrinsicParams = new ExtrinsicCameraParameters[successes];
                for (int i = 0; i < successes; i++)
                    extrinsicParams[i] = new ExtrinsicCameraParameters();

                
                CameraCalibration.CalibrateCamera(object_points, image_points, board_sz, intrinsicParam, 
                    CALIB_TYPE.DEFAULT, out extrinsicParams);
                return true;
            }

            return false;
        }

        public Image<Bgr, byte> UndistortFunc(Bitmap sourceImage)
        {
            Image<Bgr, Byte> image = new Image<Bgr, byte>(sourceImage);//capture.QueryFrame();

            Matrix<float>[] undistortMap = this.getUndistortMap(intrinsicParam, image);

            return Undistort(image, undistortMap);
        }

        private string[] promptToArray(string prompt = "")
        {
            if (prompt.Length > 1)
                Console.WriteLine(prompt);
            string rawargs = Console.ReadLine();
            return rawargs.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public void toXML(string path)
        {
            XmlTextWriter xw = new XmlTextWriter(path, Encoding.UTF8);
            StringBuilder sb = new StringBuilder();
            (new XmlSerializer(typeof(IntrinsicCameraParameters))).Serialize(new StringWriter(sb), intrinsicParam);
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(sb.ToString());
            xDoc.Save(xw);
        }

        public Matrix<float>[] getUndistortMap(IntrinsicCameraParameters intrParam, Image<Bgr, byte> sampleImage)
        {
            Matrix<float> mapx, mapy = new Matrix<float>(sampleImage.Width, sampleImage.Height);
            intrParam.InitUndistortMap(sampleImage.Width, sampleImage.Height, out mapx, out mapy);
            Matrix<float>[] undistortMap = { mapx, mapy };
            return undistortMap;
        }

        public Image<Bgr, byte> Undistort(Image<Bgr, byte> image, Matrix<float>[] undistortMap)
        {
            Image<Bgr, Byte> t = image.Clone();
            CvInvoke.cvRemap(image.Ptr, t.Ptr, undistortMap[0].Ptr, undistortMap[1].Ptr, 8, new MCvScalar(0));
            return t;
        }
    }
}
