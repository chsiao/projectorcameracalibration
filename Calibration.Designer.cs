﻿namespace Calibration
{
    partial class Calibration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkDepth = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkRigisterToRGB = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(640, 480);
            this.panel1.TabIndex = 3;
            this.panel1.Visible = false;
            // 
            // chkDepth
            // 
            this.chkDepth.AutoSize = true;
            this.chkDepth.Checked = true;
            this.chkDepth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDepth.Location = new System.Drawing.Point(6, 19);
            this.chkDepth.Name = "chkDepth";
            this.chkDepth.Size = new System.Drawing.Size(55, 17);
            this.chkDepth.TabIndex = 4;
            this.chkDepth.Text = "Depth";
            this.chkDepth.UseVisualStyleBackColor = true;
            this.chkDepth.CheckedChanged += new System.EventHandler(this.chkDepth_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkRigisterToRGB);
            this.groupBox1.Controls.Add(this.chkDepth);
            this.groupBox1.Location = new System.Drawing.Point(658, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 480);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // chkRigisterToRGB
            // 
            this.chkRigisterToRGB.AutoSize = true;
            this.chkRigisterToRGB.Location = new System.Drawing.Point(18, 42);
            this.chkRigisterToRGB.Name = "chkRigisterToRGB";
            this.chkRigisterToRGB.Size = new System.Drawing.Size(103, 17);
            this.chkRigisterToRGB.TabIndex = 5;
            this.chkRigisterToRGB.Text = "Register to RGB";
            this.chkRigisterToRGB.UseVisualStyleBackColor = true;
            this.chkRigisterToRGB.CheckedChanged += new System.EventHandler(this.chkRigisterToRGB_CheckedChanged);
            // 
            // Calibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 505);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Calibration";
            this.Text = "Calibration";
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Calibration_MouseClick);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkDepth;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkRigisterToRGB;
    }
}