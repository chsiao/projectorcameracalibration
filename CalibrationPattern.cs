﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Calibration
{
    
    public delegate void Painted();
    public partial class CalibrationPattern : Form
    {
        public event Painted AfterPaint;

        Bitmap patt1, patt2, patt3, patt4;
        public int showingPattID{ get; set;}

        public CalibrationPattern()
        {            
            InitializeComponent();
            patt1 = new Bitmap("pattern1_100x200.png");
            patt2 = new Bitmap("pattern2_350x250.png");
            patt3 = new Bitmap("pattern3_600x250.png");
            patt4 = new Bitmap("pattern4_400x350.png");
            showingPattID = 1;            
        }

        private void CalibrationPattern_Load(object sender, EventArgs e)
        {



        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            this.Focus();
            Point origin = new Point(0,0);
            Rectangle rect = new Rectangle(origin, new Size(1024, 768));
            switch(showingPattID)
            {
                case 0:
                    this.BackColor = Color.White;
                    break;
                case 1: case 2: case 3:
                    e.Graphics.DrawImage(patt1, rect);
                    break;
                case 4: case 5: case 6:
                    e.Graphics.DrawImage(patt2, rect);
                    break;
                case 7: case 8: case 9:
                    e.Graphics.DrawImage(patt3, rect);
                    break;
                case 10: case 11: case 12:
                    e.Graphics.DrawImage(patt4, rect);
                    break;                
                case 13:
                    this.BackColor = Color.Red;
                    break;
            }

            base.OnPaint(e);            
        }

    }
}
