﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Drawing.Imaging;
using NITE;
using OpenNI;

namespace Calibration
{
    public partial class Calibration : Form
	{
        private readonly string SAMPLE_XML_FILE = @"SamplesConfig.xml";
        private bool isRGBChecked = false;
        private bool isRGBCalibrationChecked = false;
        private bool isCalibrating = false;
        private bool isCalibrated = false;
        private Context context;
        private ScriptNode scriptNode;
        private DepthGenerator depth;
        private ImageGenerator image;
        private Thread readerThread;
        private bool shouldRun;
        private Bitmap bitmap;
        private int[] histogram;

        private Calibrate calibrate;
        private string[] ChessParameters = { "8", "6", "10" };

        

		public Calibration()
		{
			InitializeComponent();

            

			this.context = Context.CreateFromXmlFile(SAMPLE_XML_FILE, out scriptNode);
			this.depth = context.FindExistingNode(NodeType.Depth) as DepthGenerator;
            this.image = context.FindExistingNode(NodeType.Image) as ImageGenerator;

			if (this.depth == null)
			{
				throw new Exception("Viewer must have a depth node!");
			}

			this.histogram = new int[this.depth.DeviceMaxDepth];
			MapOutputMode mapMode = this.depth.MapOutputMode;

			this.bitmap = new Bitmap((int)mapMode.XRes, (int)mapMode.YRes, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			this.shouldRun = true;
			this.readerThread = new Thread(ReaderThread);
			this.readerThread.Start();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			lock (this)
			{
				e.Graphics.DrawImage(this.bitmap,
					this.panel1.Location.X,
					this.panel1.Location.Y,
					this.panel1.Size.Width,
					this.panel1.Size.Height);
			}
		}

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			//Don't allow the background to paint
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			this.shouldRun = false;
			this.readerThread.Join();
			base.OnClosing(e);
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			if (e.KeyChar == 27)
			{
				Close();
			}
			base.OnKeyPress(e);
		}

		private unsafe void CalcHist(DepthMetaData depthMD)
		{
			// reset
			for (int i = 0; i < this.histogram.Length; ++i)
				this.histogram[i] = 0;

			ushort* pDepth = (ushort*)depthMD.DepthMapPtr.ToPointer();

			int points = 0;
			for (int y = 0; y < depthMD.YRes; ++y)
			{
				for (int x = 0; x < depthMD.XRes; ++x, ++pDepth)
				{
					ushort depthVal = *pDepth;
					if (depthVal != 0)
					{
						this.histogram[depthVal]++;
						points++;
					}
				}
			}

			for (int i = 1; i < this.histogram.Length; i++)
			{
				this.histogram[i] += this.histogram[i-1];
			}

			if (points > 0)
			{
				for (int i = 1; i < this.histogram.Length; i++)
				{
					this.histogram[i] = (int)(256 * (1.0f - (this.histogram[i] / (float)points)));
				}
			}
		}

		private unsafe void ReaderThread()
		{
			DepthMetaData depthMD = new DepthMetaData();

			while (this.shouldRun)
			{
                lock (this)
                {
                    try
                    {
                        if (!isRGBChecked)
                            this.context.WaitOneUpdateAll(this.depth);
                        else
                            this.context.WaitOneUpdateAll(this.image);
                    }
                    catch (Exception)
                    {
                    }


                    this.depth.GetMetaData(depthMD);

                    CalcHist(depthMD);

                    Rectangle rect = new Rectangle(0, 0, this.bitmap.Width, this.bitmap.Height);
                    BitmapData data = this.bitmap.LockBits(rect, ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);


                    if (!isRGBChecked)
                    {
                        ushort* pDepth = (ushort*)this.depth.DepthMapPtr.ToPointer();

                        // set pixels
                        for (int y = 0; y < depthMD.YRes; ++y)
                        {
                            byte* pDest = (byte*)data.Scan0.ToPointer() + y * data.Stride;
                            for (int x = 0; x < depthMD.XRes; ++x, ++pDepth, pDest += 3)
                            {
                                byte pixel = (byte)this.histogram[*pDepth];
                                pDest[0] = 0;
                                pDest[1] = pixel;
                                pDest[2] = pixel;
                            }
                        }
                    }
                    else
                    {
                        byte* pImage = (byte*)this.image.ImageMapPtr.ToPointer();
                        byte* pDest = (byte*)data.Scan0.ToPointer();

                        for (int index = 0; index < (rect.Width * rect.Height); index++)
                        {
                            pDest[0] = pImage[2];
                            pDest[1] = pImage[1];
                            pDest[2] = pImage[0];
                            pDest += 3;
                            pImage += 3;
                        }
                    }


                    this.bitmap.UnlockBits(data);

                    if (chkRigisterToRGB.Checked && this.isCalibrating)
                    {
                        Emgu.CV.Image<Emgu.CV.Structure.Gray, Byte> targetImage = new Emgu.CV.Image<Emgu.CV.Structure.Gray, byte>(bitmap);
                        if (calibrate.GenerateIntrinsicExtrinsicCameraParameters(this.bitmap, out targetImage))
                        {
                            this.bitmap = calibrate.UndistortFunc(this.bitmap).ToBitmap(bitmap.Width, bitmap.Height);
                            isCalibrated = true;
                        }
                        else
                        {
                            this.bitmap = targetImage.ToBitmap();
                        }

                        this.isCalibrating = false;
                    }

                    if (isCalibrated)
                        this.bitmap = calibrate.UndistortFunc(this.bitmap).ToBitmap(bitmap.Width, bitmap.Height);

                }
				this.Invalidate();
			}
		}

        delegate void sendFlagToEnable();

        #region UI Event Listener

        void chkDepth_CheckedChanged(object sender, System.EventArgs e)
        {
            if (this.chkDepth.Checked)
            {
                this.isRGBChecked = false;
                this.chkRigisterToRGB.Enabled = true;
            }
            else
            {
                this.isRGBChecked = true;
                this.chkRigisterToRGB.Enabled = false;
            }
        }

        void Calibration_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (isRGBCalibrationChecked)
            {
                this.chkDepth.Checked = false;
                this.isCalibrating = true;
            }
        }

        void chkRigisterToRGB_CheckedChanged(object sender, System.EventArgs e)
        {
            lock (this)
            {
                if (this.chkRigisterToRGB.Checked)
                    this.depth.AlternativeViewpointCapability.SetViewpoint(this.image);
                else
                    this.depth.AlternativeViewpointCapability.ResetViewpoint();
            }
        }


        void btStoreRGBParameters_Click(object sender, System.EventArgs e)
        {
            this.calibrate.toXML(Application.ExecutablePath + "RGBtoDepthIntrinsicParameters.xml");
        }

        #endregion
    }
}
