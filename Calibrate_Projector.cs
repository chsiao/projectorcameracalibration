﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using NITE;
using OpenNI;

namespace Calibration
{
    public partial class Calibrate_Projector : Form
    {
        private const float SQUARE_SIZE = 0.039f;
        private const int PattPixel = 40;
        private const int Patt3x3Width = 8;
        private const int Patt3x3Height = 6;
        private const int Patt3x4Width = 8;
        private const int Patt3x4Height = 6;
        private const int Patt3x5Width = 8;
        private const int Patt3x5Height = 6;
        private const int Patt3x6Width = 8;
        private const int Patt3x6Height = 6;
        private Point Offset1 = new Point(140, 240); //140,240 564, 288
        private Point Offset2 = new Point(390, 290); //390,290 314, 238
        private Point Offset3 = new Point(640, 290); //640,290 64, 238
        private Point Offset4 = new Point(440, 390); //440,390 264, 138
        private const int ImageNumber = 12;
        private Size ImageSize = new Size(640, 480);
        private Size ProjectSize = new Size(1024, 768);
        //Todo: need to specify the file name
        private const string PatternFileName = "";
        private Panel panel1;

        IntrinsicCameraParameters intrinsicRGBCamera;
        
        private Context context;
        private ScriptNode scriptNode;
        private readonly string SAMPLE_XML_FILE = @"SamplesConfig.xml";
        private ImageGenerator imageGen;
        private bool shouldRun;
        private Thread readerThread;
        private Bitmap bitmap;

        private List<Image<Bgr, byte>> printedPatterns = new List<Image<Bgr, byte>>();
        private List<Image<Bgr, byte>> projectedPatterns = new List<Image<Bgr, byte>>();
        private Panel panel2;
        private Panel panel3;

        private CalibrationPattern calibrationPatt = new CalibrationPattern();

        public Calibrate_Projector()
        {
            InitializeComponent();

            Screen myScreen = Screen.FromControl(this);
            List<Screen> otherScreen = (Screen.AllScreens.Where(s => s.DeviceName != myScreen.DeviceName)).ToList<Screen>();
            //               ?? myScreen;
            if (otherScreen.Count > 0)
            {                
                calibrationPatt.StartPosition = FormStartPosition.Manual;
                calibrationPatt.TopMost = true;
                calibrationPatt.Location = new Point(otherScreen[0].WorkingArea.X, otherScreen[0].WorkingArea.Y);                
            }
            calibrationPatt.Show();
            

            this.context = Context.CreateFromXmlFile(SAMPLE_XML_FILE, out scriptNode);
            this.imageGen = context.FindExistingNode(NodeType.Image) as ImageGenerator;
            this.bitmap = new Bitmap(640, 480, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            
            /// runing the image capturing process
            
            this.shouldRun = true;
            this.readerThread = new Thread(ReaderThread);
            this.readerThread.Start();
            
            this.intrinsicRGBCamera = new IntrinsicCameraParameters();
            Matrix<double> intrinsicMatrix = new Matrix<double>(3, 3);
            intrinsicMatrix[0, 0] = 529.20168;//568.481;//5.2801440429687500e+002;
            intrinsicMatrix[0, 1] = 0.0;//0;
            intrinsicMatrix[0, 2] = 301.79034;//322.934;//320;
            intrinsicMatrix[1, 0] = 0.0;//0;
            intrinsicMatrix[1, 1] = 521.98034;//564.0856;//5.2801440429687500e+002;
            intrinsicMatrix[1, 2] = 265.50139;//255.00133;//267;
            intrinsicMatrix[2, 0] = 0;
            intrinsicMatrix[2, 1] = 0;
            intrinsicMatrix[2, 2] = 1;

            Matrix<double> distorcoeffMatrix = new Matrix<double>(5, 1);
            distorcoeffMatrix[0, 0] = 0.26876;//0.226;//1.356;//0;
            distorcoeffMatrix[1, 0] = -1.08324;//-0.566;//-17.715;//0;
            distorcoeffMatrix[2, 0] = -0.00370;//0.003674;//0.0;//-0.074;//0;
            distorcoeffMatrix[3, 0] = -0.01723;//-0.0167;//0.0;//-0.007;//0;
            distorcoeffMatrix[4, 0] = 1.61223;//3.96690;//0.0;//82.1025;//0;

            this.intrinsicRGBCamera.DistortionCoeffs = distorcoeffMatrix;
            this.intrinsicRGBCamera.IntrinsicMatrix = intrinsicMatrix;

            //Load the chessboard pattern and kinect intrinsic/extrinsic parameters
            /*
            context.images_dir = QDir(context.opt_image_directory());
            ntk_ensure(context.images_dir.exists(), (context.images_dir.absolutePath() + " is not a directory.").toAscii());
            context.images_list = context.images_dir.entryList(QStringList("view????"), QDir.Dirs, QDir.Name);

            RGBDCalibration calib = new RGBDCalibration();
            calib.loadFromFile(context.opt_input_file());
            context.rgb_intrinsics = calib.rgb_intrinsics;
            context.rgb_distortion = calib.rgb_distortion;
            context.rgb_R = calib.R;
            context.rgb_T = calib.T;
            */


            #region projector calibration
            /// load the existing images on the disk
            //for (int i = 1; i < 5; i++)
            //{
            //    Bitmap tmpBitmap1 = new Bitmap("OriginalTmpBitmap1-" + i + ".bmp");
            //    projectedPatterns.Add(new Image<Bgr, byte>(tmpBitmap1));

            //    Bitmap tmpBitmpa2 = new Bitmap("OriginalTmpBitmap2-" + i + ".bmp");
            //    printedPatterns.Add(new Image<Bgr, byte>(tmpBitmap2));
            //}

            //CameraProjectorCalibration();
            #endregion         
        }                         

        private unsafe void ReaderThread()
        {
            DepthMetaData depthMD = new DepthMetaData();

            while (this.shouldRun)
            {
                lock (this)
                {
                    Rectangle rect = new Rectangle(0, 0, this.bitmap.Width, this.bitmap.Height);
                    BitmapData data = this.bitmap.LockBits(rect, ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                    byte* pImage = (byte*)this.imageGen.ImageMapPtr.ToPointer();
                    byte* pDest = (byte*)data.Scan0.ToPointer();

                    for (int index = 0; index < (rect.Width * rect.Height); index++)
                    {
                        pDest[0] = pImage[2];
                        pDest[1] = pImage[1];
                        pDest[2] = pImage[0];
                        pDest += 3;
                        pImage += 3;
                    }

                    this.bitmap.UnlockBits(data);

                }

                this.Invalidate();
            }
        }

        /// <summary>
        /// 1. Calculate the real pattern corners points on printed pattern
        /// 2. load the pattern file
        /// 3. undistorted the loaded image based on the camera's intrinsic parameters
        /// 4. create the homography based on comparing the undistorted corners and the ideal corners
        /// </summary>
        /// <param name="homographies"></param>
        private void computeHomographies_rgb(ref List<HomographyMatrix> homographies, ref List<PointF[]> detectedCameraCorners)
        {
            PointF[] pattern_points = new PointF[Patt3x3Width*Patt3x3Height];
            projectorCalibrationPattern(ref pattern_points);

            List<List<PointF>> good_corners = new List<List<PointF>>();
            //homographies.resize(context.images_list.size());

            for (int i_image = 0; i_image < this.printedPatterns.Count; ++i_image)
            {
                // load the calibrating image file from disk                
                Image<Bgr, byte> image = printedPatterns[i_image];                

                // undistort the loaded image
                Matrix<float>[] undistortMap = this.getUndistortMap(this.intrinsicRGBCamera, image);
                Image<Bgr, byte> undistorted_image = Undistort(image, undistortMap);

                List<PointF> current_view_corners = new List<PointF>();

                // find the chesboard
                bool found = detectPrintedChessboards(PatternFileName, undistorted_image, ref current_view_corners);

                if (found)
                {                    
                    // comparing the actual undistorted pattern corners to the ideal pattern corners to get the homography matrix
                    HomographyMatrix tmpHomography = CameraCalibration.FindHomography(current_view_corners.ToArray(), pattern_points, HOMOGRAPHY_METHOD.RANSAC, 0.01);
                    detectedCameraCorners.Add(current_view_corners.ToArray());

                    if (tmpHomography != null)
                        homographies.Add(tmpHomography);                                                           
                }
                
            }
        }

        /// <summary>
        /// 1. load the captured image of projected pattern from the disk
        /// 2. undistorted the image based on the RGB camera intrinsic paramter
        /// 3. return the corners from those valid undistorted RGB image
        /// </summary>
        /// <param name="stereo_corners"></param>
        private void undistort_projector(ref PointF[][] stereo_corners)
        {
            //stereo_corners.resize(context.images_list.size());

            for (int i_image = 0; i_image < this.projectedPatterns.Count; ++i_image)
            {
                // load the calibrating image file from disk                
                Image<Bgr, byte> image = this.projectedPatterns[i_image];
                //undistort(image, undistorted_image, context.rgb_intrinsics, context.rgb_distortion);
                Matrix<float>[] undistortMap = this.getUndistortMap(this.intrinsicRGBCamera, image);
                Image<Bgr, byte> undistorted_image = Undistort(image, undistortMap);

                PointF[] current_view_corners = new PointF[0];
                calibrationCorners(Patt3x3Width, Patt3x3Height, ref current_view_corners, ref undistorted_image, 1.0f);                

                // get the valid corners
                if (current_view_corners.Length == (Patt3x3Width * Patt3x3Height))
                {
                    stereo_corners[i_image] = current_view_corners;
                }
                else
                {
                    stereo_corners[i_image] = new PointF[0];
                }
            }
        }

        private void calibrate_projector(List<HomographyMatrix> homographies, PointF[][] undistorted_proj_corners, List<PointF[]> camCorners)
        {            
            List<List<PointF>> proj_patterns = new List<List<PointF>>();
            List<PointF[]> undistorted_good_rgb = new List<PointF[]>();

            // find the projected points based on camera view to white board plane homography
            // The found projected pattern can be the object points for camera(projector) calibration task
            // The displaying pattern from projector is actually the view point for the camera calibration
            int i = 0;
            foreach (PointF[] corners in undistorted_proj_corners)
            {
                if (homographies[i] != null && corners.Length > 0)
                {
                    undistorted_good_rgb.Add(corners);
                    
                    PointF[] newCorners = (PointF[])corners.Clone();
                    homographies[i].ProjectPoints(newCorners);

                    PointF[] oneFrameCamCorner = camCorners[i];
                    homographies[i].ProjectPoints(oneFrameCamCorner);

                    proj_patterns.Add(newCorners.ToList());
                }
                i++;
            }
            
            // Allocating the actual objects points back to the view points of projector
            MCvPoint3D32f[][] proj_pattern_3d = new MCvPoint3D32f[proj_patterns.Count][];
            i = 0;
            foreach (List<PointF> proj_pattern in proj_patterns)
            {
                proj_pattern_3d[i] = new MCvPoint3D32f[proj_pattern.Count];
                int j = 0;
                foreach (PointF patternPoint in proj_pattern)
                {
                    proj_pattern_3d[i][j].x = patternPoint.X;
                    proj_pattern_3d[i][j].y = patternPoint.Y;
                    proj_pattern_3d[i][j].z = 0;
                    
                    j++;
                }
                i++;
            }

            // When we pretend the projector as a camera, we can treat this undistorted_good_proj as the view of the projector
            PointF[][] undistorted_good_proj = new PointF[proj_patterns.Count][];
            find_projector_image(proj_patterns.Count, ref undistorted_good_proj);
            
            ExtrinsicCameraParameters[] projectorExtrinsic = new ExtrinsicCameraParameters[1];
            IntrinsicCameraParameters projectorIntrinsic = new IntrinsicCameraParameters();

            // Find the projector's intrinsic/extrinsic parameters
            double errorInt = CameraCalibration.CalibrateCamera(proj_pattern_3d, undistorted_good_proj, ProjectSize, projectorIntrinsic,
                CALIB_TYPE.CV_CALIB_ZERO_TANGENT_DIST, out projectorExtrinsic);        

            // declare the parameters for stereo calibration
            ExtrinsicCameraParameters extrinsic = new ExtrinsicCameraParameters();
            Matrix<double> foundamentalMatrix;
            Matrix<double> essentialMatrix;
            
            // Proj_pattern_3d are the actual points of the projected image, undistorted_good_proj are the points that are saw by projector
            // undistorted_good_rgb is the points that are saw by the camera.
            CameraCalibration.StereoCalibrate(proj_pattern_3d, undistorted_good_proj, undistorted_good_rgb.ToArray(),
                projectorIntrinsic, this.intrinsicRGBCamera, ProjectSize, CALIB_TYPE.CV_CALIB_USE_INTRINSIC_GUESS,//.CV_CALIB_FIX_FOCAL_LENGTH,
                new MCvTermCriteria(100, 0.01), out extrinsic, out foundamentalMatrix, out essentialMatrix);

            double error = computeError(foundamentalMatrix, undistorted_good_proj, undistorted_good_rgb.ToArray());

            toXML("8.xml", projectorIntrinsic, extrinsic, foundamentalMatrix, essentialMatrix);
            Console.Write("Average pixel reprojection error: ");
            Console.Write(error);
            Console.Write("\n");         
        }

        public void toXML(string path, IntrinsicCameraParameters prjectorIntrinsic, ExtrinsicCameraParameters projectorExtrinsic, Matrix<double> foundamentalMatrix, Matrix<double> EssentialMatrix)
        {
            XmlTextWriter xw = new XmlTextWriter(path, Encoding.UTF8);
            FinalObject savedObject = new FinalObject();
            savedObject.CameraToProjector = projectorExtrinsic;
            savedObject.EssentialMatrix = EssentialMatrix;
            savedObject.FundamentalMatrix = foundamentalMatrix;
            savedObject.ProjectorIntrinsic = prjectorIntrinsic;

            StringBuilder sb1 = new StringBuilder();
            (new XmlSerializer(typeof(FinalObject))).Serialize(new StringWriter(sb1), savedObject);
            
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(sb1.ToString());
            xDoc.Save(xw);
        }



        private bool detectPrintedChessboards(string imageName, Image<Bgr, byte> frame, ref List<PointF> corners)
        {
            Image<Bgr, byte> copy = frame.Clone();

            // Find chessboard corners.
            PointF[] corners0 = new PointF[0];            
            
            detectSinglePrintedChessboard(imageName + ".pattern", Patt3x3Width, Patt3x3Height, ref copy, ref corners0);            
            
            corners.AddRange(corners0); 

            if (corners.Count > 0)
                return true;
            else
                // Return without errors (i.e., chessboard was found).
                return false;//ret;
        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageName">for showing the name on the interface</param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="frame"></param>
        /// <param name="corners"></param>
        private void detectSinglePrintedChessboard(string imageName, int width, int height, ref Image<Bgr, Byte> frame, ref PointF[] corners)
        {
            float minX;
            float minY;
            float maxX;
            float maxY;
            float offset = 12F;

            calibrationCorners(width, height, ref corners, ref frame, 1);

            if (corners.Length == 0)
                return;

            minX = corners.Min(p => p.X);
            maxX = corners.Max(p => p.X);
            minY = corners.Min(p => p.Y);
            maxY = corners.Max(p => p.Y);

            minX = Math.Max(minX - offset, 0f);
            maxX = Math.Min(maxX + offset, (float)ImageSize.Width);
            minY = Math.Max(minY - offset, 0f);
            maxY = Math.Min(maxY + offset, (float)ImageSize.Height);            

            //Todo: Not sure this way to draw image is right
            Rectangle rect = new Rectangle((int)minX, (int)minY, (int)(maxX - minX), (int)(maxY - minY));
            frame.Draw(rect, new Bgr(Color.Red), 3);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image_name">the name for showing the image on the interface</param>
        /// <param name="window_name"></param>
        /// <param name="pattern_width">how many corners in the width dimension of the pattern</param>
        /// <param name="pattern_height">how many corners in the height dimension of the pattern</param>
        /// <param name="corners">the corner points from image that passed in</param>
        /// <param name="image">The image for detecting the corners, it will be resized by scal_factor when finishing this method</param>
        /// <param name="scale_factor"></param>
        private Image<Gray,byte> calibrationCorners(int pattern_width, int pattern_height, ref PointF[] corners,
            ref Image<Bgr, Byte> image, float scale_factor)
        {
            Size pattern_size = new Size(pattern_width, pattern_height);

            Image<Bgr, Byte> scaled_image = new Image<Bgr, byte>((int)(image.Width * scale_factor), (int)(image.Height * scale_factor));

            if (scale_factor == 1f)
            {
                scaled_image = image.Clone();
            }
            else
            {
                CvInvoke.cvResize(image.Ptr, scaled_image.Ptr, INTER.CV_INTER_CUBIC);
            }

            //ok = CvInvoke.cvFindChessboardCorners(scaled_image.Ptr, pattern_size, corners, flags);
            Image<Gray, Byte> scaled_Gray_Image = scaled_image.Convert<Gray, byte>();
            //this.bitmap = scaled_Gray_Image.ToBitmap();
            corners = CameraCalibration.FindChessboardCorners(scaled_Gray_Image, pattern_size, CALIB_CB_TYPE.DEFAULT);
            if (corners == null || corners.Length == 0)
            {
                corners = CameraCalibration.FindChessboardCorners(scaled_Gray_Image, pattern_size, CALIB_CB_TYPE.NORMALIZE_IMAGE);
                if (corners == null || corners.Length == 0)
                    corners = CameraCalibration.FindChessboardCorners(scaled_Gray_Image, pattern_size, CALIB_CB_TYPE.ADAPTIVE_THRESH);
            }
            if (corners != null)
            {
                CameraCalibration.DrawChessboardCorners(scaled_Gray_Image, pattern_size, corners);                
                scaled_Gray_Image.FindCornerSubPix(new PointF[][] { corners }, new Size(5, 5), new Size(-1, -1), new MCvTermCriteria(30, 0.1));                                

                //Todo: assign the corners point to somewhere
                for (int i = 0; i < corners.Length; ++i)
                {
                    corners[i].X /= scale_factor;
                    corners[i].Y /= scale_factor;
                }
                return scaled_Gray_Image;
            }
            else
            {
                corners = null;
                return null;
            }
        }

        public Matrix<float>[] getUndistortMap(IntrinsicCameraParameters intrParam, Image<Bgr, byte> sampleImage)
        {
            Matrix<float> mapx, mapy = new Matrix<float>(sampleImage.Width, sampleImage.Height);
            intrParam.InitUndistortMap(sampleImage.Width, sampleImage.Height, out mapx, out mapy);
            Matrix<float>[] undistortMap = { mapx, mapy };
            return undistortMap;
        }

        public Image<Bgr, byte> Undistort(Image<Bgr, byte> image, Matrix<float>[] undistortMap)
        {
            Image<Bgr, Byte> t = image.Clone();
            CvInvoke.cvRemap(image.Ptr, t.Ptr, undistortMap[0].Ptr, undistortMap[1].Ptr, 8, new MCvScalar(0));
            return t;
        }

        private void find_projector_image(int nImages, ref PointF[][] corners)
        {
            Point[] OffsetPoints = new Point[] { Offset1, Offset1, Offset1,
                Offset2, Offset2, Offset2, Offset3, Offset3, Offset3,
                Offset4, Offset4, Offset4};
            MCvPoint3D32f[][] corners3d = new MCvPoint3D32f[nImages][];
            calibrationPattern(ref corners3d, Patt3x3Width, Patt3x3Height, PattPixel, nImages);

            corners = new PointF[corners3d.Length][];

            for (int i = 0; i < corners3d.Length; i++)
            {

                corners[i] = new PointF[corners3d[i].Length];

                for (int j = 0; j < corners3d[i].Length; j++)
                {
                    corners[i][j].X = corners3d[i][j].x + OffsetPoints[i].X; //corners3d[i][corners3d[i].Length - 1 - j].x + OffsetPoints[i].X;
                    corners[i][j].Y = corners3d[i][j].y + OffsetPoints[i].Y; //corners3d[i][corners3d[i].Length - 1 - j].y + OffsetPoints[i].Y;
                }
            }
        }

        private double computeError(Matrix<double> F, PointF[][] rgb_corners, PointF[][] depth_corners)
        {
            int rgbPointCount = 0;
            for (int i = 0; i < rgb_corners.Length; ++i)
                for (int j = 0; j < rgb_corners[i].Length; ++j)
                    rgbPointCount++;

            Matrix<float> points_in_rgb = new Matrix<float>(rgbPointCount, 2);
            rgbPointCount = 0;
            for (int i = 0; i < rgb_corners.Length; ++i)
                for (int j = 0; j < rgb_corners[i].Length; ++j)
                {
                    points_in_rgb[rgbPointCount, 0] = rgb_corners[i][j].X;
                    points_in_rgb[rgbPointCount, 1] = rgb_corners[i][j].Y;
                    rgbPointCount++;
                }

            int depthPointCount = 0;
            for (int i = 0; i < depth_corners.Length; ++i)
                for (int j = 0; j < depth_corners[i].Length; ++j)
                    depthPointCount++;
            Matrix<float> points_in_depth = new Matrix<float>(depthPointCount, 2);
            depthPointCount = 0;
            for (int i = 0; i < depth_corners.Length; ++i)
                for (int j = 0; j < depth_corners[i].Length; ++j)
                {
                    points_in_depth[depthPointCount, 0] = depth_corners[i][j].X;
                    points_in_depth[depthPointCount, 1] = depth_corners[i][j].Y;
                    depthPointCount++;
                }

            //Todo: the array counts might be different
            Matrix<float> lines_in_depth = new Matrix<float>(depthPointCount, 3);
            Matrix<float> lines_in_rgb = new Matrix<float>(rgbPointCount, 3);

            CvInvoke.cvComputeCorrespondEpilines(points_in_rgb.Ptr, 1, F.Ptr, lines_in_depth.Ptr);
            CvInvoke.cvComputeCorrespondEpilines(points_in_depth.Ptr, 2, F.Ptr, lines_in_rgb.Ptr);            

            double avgErr = 0;
            for (int i = 0; i < rgbPointCount; ++i)
            {
                double err = Math.Abs(points_in_rgb[i, 0] * lines_in_rgb[i, 0] + points_in_rgb[i, 1] * lines_in_rgb[i, 1] + lines_in_rgb[i, 2]);
                avgErr += err;
            }

            for (int i = 0; i < depthPointCount; ++i)
            {
                double err = Math.Abs(points_in_depth[i,0] * lines_in_depth[i,0] + points_in_depth[i,1] * lines_in_depth[i,1] + lines_in_depth[i,2]);
                avgErr += err;
            }

            return avgErr / (rgbPointCount + depthPointCount);
        }

        /// <summary>
        /// Get the ideal corner points of the projected patterns
        /// </summary>
        /// <param name="pattern_points"></param>
        private void projectorCalibrationPattern(ref PointF[] pattern_points)
        {            
            MCvPoint3D32f[][] pattern_points_3x3 = new MCvPoint3D32f[1][];            

            calibrationPattern(ref pattern_points_3x3, Patt3x3Width, Patt3x3Height, SQUARE_SIZE, 1);            

            pattern_points = new PointF[pattern_points_3x3[0].Length];
            int i = 0;            

            foreach (MCvPoint3D32f it in pattern_points_3x3[0])
            {
                pattern_points[i].X = it.x;
                pattern_points[i].Y = it.y;
                i++;
            }
        }

        private void calibrationPattern(ref MCvPoint3D32f[][] output, int pattern_width, int pattern_height, float square_size, int nb_images)
        {
            int nb_corners = pattern_width * pattern_height;
            
            output = new MCvPoint3D32f[nb_images][];
            for (int i = 0; i < nb_images; ++i)
            {
                output[i] = new MCvPoint3D32f[nb_corners];
                for (int j = 0; j < pattern_height; ++j)
                    for (int k = 0; k < pattern_width; ++k)
                    {
                        output[i][j * pattern_width + k] = new MCvPoint3D32f(k * square_size, j * square_size, 0);
                    }
            }
        }

        private void calibrationPattern(ref MCvPoint3D32f[] output, int pattern_width, int pattern_height, float square_size)
        {
            int nb_corners = pattern_width * pattern_height;


            output = new MCvPoint3D32f[nb_corners];
            for (int j = 0; j < pattern_height; ++j)
                for (int k = 0; k < pattern_width; ++k)
                {
                    output[j * pattern_width + k] = new MCvPoint3D32f(k * square_size, j * square_size, 0);
                }

        }

        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(640, 480);
            this.panel1.TabIndex = 4;
            this.panel1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(658, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(320, 240);
            this.panel2.TabIndex = 5;
            this.panel2.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(658, 252);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(320, 240);
            this.panel3.TabIndex = 6;
            this.panel3.Visible = false;
            // 
            // Calibrate_Projector
            // 
            this.ClientSize = new System.Drawing.Size(989, 504);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "Calibrate_Projector";
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Calibrate_Projector_MouseClick);
            this.ResumeLayout(false);

        }

        void Calibrate_Projector_MouseClick(object sender, MouseEventArgs e)
        {
            lock (this)
            {
                if (calibrationPatt.showingPattID < ImageNumber + 1)
                {
                    this.projectedPatterns.Add(new Image<Bgr, byte>(this.bitmap));

                    calibrationPatt.showingPattID = ImageNumber + 1;
                    this.calibrationPatt.Refresh();                    
                    isRefreshed = true;
                }
                else
                {
                    calibrationPatt.showingPattID = 0;
                    this.calibrationPatt.Refresh();

                    string message = "Proceed to Calibration?";
                    string caption = "okay?";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result;

                    result = MessageBox.Show(this, message, caption, buttons,
                            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1,
                            MessageBoxOptions.RightAlign);

                    if (result == DialogResult.No)
                    {
                        DialogResult result2;
                        string message2 = "Are you sure to redo all the capture process?";
                        string caption2 = "";
                        result2 = MessageBox.Show(this, message2, caption2, buttons,
                            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1,
                            MessageBoxOptions.RightAlign);

                        if (result2 == DialogResult.Yes)
                        {
                            redoCapturing();
                        }
                        else if (projectedPatterns.Count == 12 && printedPatterns.Count == 12)
                        {
                            CameraProjectorCalibration();
                            //SingleCameraCalibration();
                        }
                        else redoCapturing();

                    } 
                    else if (projectedPatterns.Count == 12 && printedPatterns.Count == 12)
                    {
                        CameraProjectorCalibration();
                        //SingleCameraCalibration();
                    }
                    else redoCapturing();                    
                }                
            }
        }

        private void redoCapturing()
        {
            this.printedPatterns = new List<Image<Bgr, byte>>();
            this.pattID = 1;
            this.calibrationPatt.showingPattID = pattID;
            this.calibrationPatt.Refresh();
            this.shouldRun = true;
        }

        private void SingleCameraCalibration()
        {
            ExtrinsicCameraParameters[] projectorExtrinsic = new ExtrinsicCameraParameters[1];
            IntrinsicCameraParameters projectorIntrinsic = new IntrinsicCameraParameters();

            MCvPoint3D32f[][] objectPoints = new MCvPoint3D32f[printedPatterns.Count][];
            calibrationPattern(ref objectPoints, Patt3x3Width, Patt3x3Height, 0.04f, printedPatterns.Count);
            PointF[][] corners = new PointF[printedPatterns.Count][];

            for(int i=0;i<printedPatterns.Count;i++)
            {
                PointF[] oneCorner = new PointF[Patt3x3Width*Patt3x3Height];
                Image<Bgr, byte> frame = printedPatterns[i];
                calibrationCorners(Patt3x3Width, Patt3x3Height, ref oneCorner, ref frame, 1f);
                corners[i] = oneCorner;
            }
            // Find the projector's intrinsic/extrinsic parameters
            double errorInt = CameraCalibration.CalibrateCamera(objectPoints, corners, new Size(640,480), projectorIntrinsic,
                CALIB_TYPE.DEFAULT, out projectorExtrinsic);

        }

        private void CameraProjectorCalibration()
        {
            this.shouldRun = false;

            List<PointF[]> DetectedCamCorners = new List<PointF[]>();
            List<HomographyMatrix> homographies = new List<HomographyMatrix>();
            PointF[][] undistorted_proj_corners = new PointF[ImageNumber][];

            // get the homographies between the camera and the real printed patterns
            computeHomographies_rgb(ref homographies, ref DetectedCamCorners);
            // get the undistorted corners of projected patterns
            undistort_projector(ref undistorted_proj_corners);
            // 
            calibrate_projector(homographies, undistorted_proj_corners, DetectedCamCorners);
        }

        bool isRefreshed = false;        
        int pattID = 1;
        int paintCount = 0;
        Bitmap tmpBitmap = null, tmpBitmap2 = null;
        protected override void OnPaint(PaintEventArgs e)
        {            
            lock (this)
            {                
                e.Graphics.DrawImage(this.bitmap,
                    this.panel1.Location.X,
                    this.panel1.Location.Y,
                    this.panel1.Size.Width,
                    this.panel1.Size.Height);                

                Image<Gray, byte> grayDetectedCornerPrintedPattern = null;
                Image<Gray, byte> grayDetectedCornerProjectedPattern = null;
                bool isAddingPrintedPattern = false;

                if (isRefreshed /*&& this.calibrationPatt.Focused*/ && this.calibrationPatt.BackColor == Color.Red)
                {
                    paintCount++;
                    if (paintCount > 30)
                    {                                                              
                        Image<Bgr, byte> tmpPrintedPattern = new Image<Bgr, byte>(this.bitmap);
                        
                        Matrix<float>[] undistortMap = this.getUndistortMap(this.intrinsicRGBCamera, tmpPrintedPattern);
                        Image<Bgr, byte> undistorted_image = Undistort(tmpPrintedPattern, undistortMap);

                        PointF[] corners = new PointF[Patt3x3Height * Patt3x3Width];

                        grayDetectedCornerPrintedPattern = this.calibrationCorners(Patt3x3Width, Patt3x3Height, ref corners, ref undistorted_image, 1);
                        //grayDetectedCornerPrintedPattern = this.calibrationCorners(Patt3x3Width, Patt3x3Height, ref corners, ref tmpPrintedPattern, 1);
                        // System.Diagnostics.Debug.WriteLine("paintCount: "+paintCount);
                        if (paintCount > 35 || grayDetectedCornerPrintedPattern != null)
                        {
                            printedPatterns.Add(new Image<Bgr, byte>(this.bitmap));

                            paintCount = 0;
                            isAddingPrintedPattern = true;

                            pattID++;

                            isRefreshed = false;
                            this.calibrationPatt.showingPattID = pattID;
                            this.calibrationPatt.Refresh();
                        }
                    }
                }

                if (isAddingPrintedPattern)
                {
                    if (projectedPatterns.Count > 0)
                    {
                        Image<Bgr, byte> tmpProjectedPattern = projectedPatterns.Last();

                        Matrix<float>[] undistortMap = this.getUndistortMap(this.intrinsicRGBCamera, tmpProjectedPattern);
                        Image<Bgr, byte> undistorted_image = Undistort(tmpProjectedPattern, undistortMap);

                        PointF[] corners = new PointF[Patt3x3Height * Patt3x3Width];
                        grayDetectedCornerProjectedPattern = this.calibrationCorners(Patt3x3Width, Patt3x3Height, ref corners, ref undistorted_image, 1);
                        //grayDetectedCornerProjectedPattern = this.calibrationCorners(Patt3x3Width, Patt3x3Height, ref corners, ref tmpProjectedPattern, 1);
                    }

                    if (grayDetectedCornerProjectedPattern != null)
                    {
                        tmpBitmap = grayDetectedCornerProjectedPattern.ToBitmap();
                        //tmpBitmap.Save("tmpBitmap111-" + (pattID - 1).ToString() + ".bmp");
                        //projectedPatterns.Last().ToBitmap().Save("OriginalTmpBitmap111-" + (pattID-1).ToString() + ".bmp");
                    }

                    if (grayDetectedCornerPrintedPattern != null)
                    {
                        tmpBitmap2 = grayDetectedCornerPrintedPattern.ToBitmap();
                        //tmpBitmap2.Save("tmpBitmap211-" + (pattID - 1).ToString() + ".bmp");
                        //printedPatterns.Last().ToBitmap().Save("OriginalTmpBitmap211-" + (pattID - 1).ToString() + ".bmp");
                    }
                    
                    if (grayDetectedCornerProjectedPattern == null || grayDetectedCornerPrintedPattern == null)
                    {                    
                        projectedPatterns.Remove(projectedPatterns.Last());
                        printedPatterns.Remove(printedPatterns.Last());

                        if (grayDetectedCornerProjectedPattern == null)
                            MessageBox.Show("Cannot detect the projected pattern corners");
                        if (grayDetectedCornerPrintedPattern == null)
                            MessageBox.Show("Cannot detect the printed pattern corners");

                        pattID--;
                        this.calibrationPatt.showingPattID = pattID;
                        this.calibrationPatt.Refresh();
                    }

                }

                if (tmpBitmap != null)
                {
                    e.Graphics.DrawImage(tmpBitmap,
                            this.panel2.Location.X,
                            this.panel2.Location.Y,
                            this.panel2.Size.Width,
                            this.panel2.Size.Height);                        
                }

                if (tmpBitmap2 != null)
                {
                    e.Graphics.DrawImage(tmpBitmap2,
                            this.panel3.Location.X,
                            this.panel3.Location.Y,
                            this.panel3.Size.Width,
                            this.panel3.Size.Height);
                }

                //this.Focus();                   
            }

            base.OnPaint(e);
        }

        public bool isCalibrating = false;
    }

    public class FinalObject
    {
        private ExtrinsicCameraParameters projectorToCamera;
        private IntrinsicCameraParameters projectorInstrinsic;
        private Matrix<double> fundamental;
        private Matrix<double> essential;
        public FinalObject()
        {            
        }

        public ExtrinsicCameraParameters CameraToProjector { get; set; }

        public IntrinsicCameraParameters ProjectorIntrinsic { get; set; }

        public Matrix<double> FundamentalMatrix { get; set; }

        public Matrix<double> EssentialMatrix { get; set; }
    }
}
